module.exports = (sequelize, Sequelize) => {
    const Tag_heading = sequelize.define("tag_heading", {
    tag_title: {
      type: Sequelize.STRING,
      allowNull: true,
          },
      overall_question_count: {
        type: Sequelize.INTEGER,
      },
      overall_view_count: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      overall_answer_count: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
     
    
    });
    return Tag_heading;
  };
  