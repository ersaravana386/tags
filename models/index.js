const config = require("config");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(config.DB_CONNECTION.DB, config.DB_CONNECTION.USER, config.DB_CONNECTION.PASSWORD, {
  host: config.DB_CONNECTION.HOST,
  dialect: config.DB_CONNECTION.DIALECT,
  operatorsAliases: "0",
  pool: {
    max: config.DB_CONNECTION.POOL.MAX,
    min: config.DB_CONNECTION.POOL.MIN,
    acquire: config.DB_CONNECTION.POOL.ACQUIRE,
    idle: config.DB_CONNECTION.POOL.IDLE,
  },
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.admins = require("./admin.model.js")(sequelize, Sequelize);


db.tag = require("./tag.model.js")(sequelize, Sequelize);
db.tag_heading = require("./tag_heading.model.js")(sequelize, Sequelize);








module.exports = db;

