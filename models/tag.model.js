module.exports = (sequelize, Sequelize) => {
    const Tag = sequelize.define("tag", {
    question_id: {
      type: Sequelize.INTEGER,
      allowNull: true,
          },
      tag_name: {
        type: Sequelize.TEXT('long')
      },
      view_count: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
      answer_count: {
        type: Sequelize.INTEGER,
        allowNull: true,
      },
     
    
    });
    return Tag;
  };
  