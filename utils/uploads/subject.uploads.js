const multer = require("multer");
var mkdirp = require('mkdirp');
const pify = require("pify");
const path = require("path");
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        mkdirp.sync("public/files/subjects/");
        cb(null, "public/files/subjects/");
    },
    filename: (req, file, cb) => {
        var ext = path.extname(file.originalname);
        var filename = "subject" + "-" + Date.now() + ext;
        cb(null, filename);
    }
});
let upload = pify(multer({ storage: storage }).fields([{ name: "uploaded_image" }]));
module.exports = upload;