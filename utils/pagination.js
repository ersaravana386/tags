const url = require("url");

let pagination = async (req, res) => {
  let { pathname, query } = url.parse(req.url, true);
  let page = res.pagination.page ? res.pagination.page + 1 : 1;
  let active_pages = [];

  let totalpages = Math.ceil(
    res.pagination.totalCount / res.pagination.pageSize
  );

  if (totalpages == 1) {
    return "";
  }

  let flag = 0;
  for (var i = 1; i <= totalpages; ) {
    if (flag == 1) {
      break;
    }
    active_pages = [];
    for (var j = 0; j < 5 && i <= totalpages; j++) {
      if (page == i) {
        flag = 1;
      }
      active_pages.push(i);
      i++;
    }
  }

  let pages = "";
  let current_page = 0;
  for (i = 0; i < active_pages.length; i++) {
    let active = "";
    if (page == active_pages[i]) {
      active = "active";
      current_page = active_pages[i];
    }
    query.page = active_pages[i];
    let link = new URLSearchParams(query).toString();
    pages += `<li class="page-item ${active}"><a class="page-link" href="${
      pathname + "?" + link
    }">${active_pages[i]}</a></li>`;
  }

  //first page
  query.page = 1;
  link = new URLSearchParams(query).toString();
  let first_page = `
  <li class="page-item">
    <a class="page-link" href="${pathname + "?" + link}" aria-label="First">
      <i class="fa fa-angle-double-left"></i>
    </a>
  </li>`;
  //end first page

  //last page
  query.page = totalpages;
  link = new URLSearchParams(query).toString();
  let last_page = `<li class="page-item">
    <a class="page-link" href="${pathname + "?" + link}" aria-label="Last">
      <i class="fa fa-angle-double-right"></i>
    </a>
  </li>`;
  //end lastpage

  //prev page
  let prev_page = "";
  if (current_page > 1) {
    query.page = current_page - 1;
    link = new URLSearchParams(query).toString();
    prev_page = `<li class="page-item">
      <a class="page-link" href="${pathname + "?" + link}" aria-label="Prev">
        <i class="fa fa-angle-left"></i>
      </a>
    </li>`;
  }
  //end prev page

  //next page
  let next_page = "";
  if (current_page < totalpages) {
    query.page = current_page + 1;
    link = new URLSearchParams(query).toString();
    next_page = `<li class="page-item">
      <a class="page-link" href="${pathname + "?" + link}" aria-label="Prev">
        <i class="fa fa-angle-right"></i>
      </a>
    </li>`;
  }
  //end next page

  paginationHtml = `<div class="ht-80 d-flex align-items-center justify-content-center">
    <nav aria-label="Page navigation">
      <ul class="pagination pagination-basic mg-b-0">
        ${first_page}
        ${prev_page}
        ${pages}
        ${next_page}
        ${last_page}
      </ul>
    </nav>
  </div>`;

  res.locals.links = paginationHtml;
  return true;
};

module.exports = pagination;
