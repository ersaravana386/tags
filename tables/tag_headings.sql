-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 08, 2021 at 04:08 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beyondtheory`
--

-- --------------------------------------------------------

--
-- Table structure for table `tag_headings`
--

CREATE TABLE `tag_headings` (
  `id` int(11) NOT NULL,
  `tag_title` varchar(255) DEFAULT NULL,
  `overall_question_count` int(11) DEFAULT NULL,
  `overall_view_count` int(11) DEFAULT NULL,
  `overall_answer_count` int(11) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tag_headings`
--

INSERT INTO `tag_headings` (`id`, `tag_title`, `overall_question_count`, `overall_view_count`, `overall_answer_count`, `createdAt`, `updatedAt`) VALUES
(1, 'node.js', 30, 79345, 52, '2021-02-07 07:16:28', '2021-02-07 07:16:28'),
(2, 'php', 30, 68917, 117, '2021-02-07 07:17:04', '2021-02-07 07:17:51'),
(3, '.net', 30, 49680, 81, '2021-02-07 07:18:56', '2021-02-07 07:18:56'),
(4, 'laravel', 30, 271749, 49, '2021-02-07 07:19:20', '2021-02-07 07:19:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tag_headings`
--
ALTER TABLE `tag_headings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tag_headings`
--
ALTER TABLE `tag_headings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
