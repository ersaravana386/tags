let express = require("express");
let router = express.Router();

let loginController = require("../controllers/web/login.controller");
let dashboardController = require("../controllers/web/dashboard.controller");

//login
router.get("/", loginController.login);
//dashboard
router.get("/dashboard", dashboardController.dashboard);


router.post("/tag_register", dashboardController.tag_register);
router.get("/overall_count_details", dashboardController.overall_count_details);

router.post("/login_check", loginController.login_check);




module.exports = router;
