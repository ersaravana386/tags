const db = require("../models/index");

const Customer = db.customers;

var isLoggedIn = async (req, res, next) => {
  let auth = req.header("authorization") || null;
  let customer = await Customer.findOne({
    where: {
      api_token: auth,
    },
  });
  if (!customer) {
    return res.json({
      status: false,
      message: "Invalid api key",
    });
  }
  req.user = customer;
  next();
};
module.exports = isLoggedIn;
