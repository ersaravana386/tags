require("dotenv").config();
const config = require("config");

var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var logger = require("morgan");
var mysql = require("mysql2");
var indexRouter = require("./routes/index");

var app = express();

//Murugan Start
var session = require("express-session");
var MemoryStore = require("memorystore")(session);

app.set("trust proxy", 1); // trust first proxy
app.use(
  session({
    secret: "keyboard cat",
    resave: false,
    saveUninitialized: true,
    cookie: {
      maxAge: 60000,
      secure: config.SECURE,
    },
    store: new MemoryStore({
      checkPeriod: 86400000, // prune expired entries every 24h
    }),
  })
);

var connection = mysql.createConnection({
  host: config.DB_CONNECTION.HOST,
  user: config.DB_CONNECTION.USER,
  password: config.DB_CONNECTION.PASSWORD,
  database: config.DB_CONNECTION.DB,
});

connection.connect((err) => {
  if (err) throw err;
  console.log("Connected!");
});

global.db = connection;

//Murugan End

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.use(logger("dev"));

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

app.use(cookieParser());
app.use(express.static(path.join(__dirname, "public")));

//set global variables
app.use(function (req, res, next) {
  res.locals = {
    siteTitle: config.SITE_TITLE,
    ss3Link: config.SS3_LINK,
    pageSize: config.PAGE_SIZE,
    paginationSize: config.PAGINATION_SIZE,
  };
  res.MY_NAMESPACE = config.MY_NAMESPACE;
  next();
});

app.use("/", indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

const db = require("./models");
db.sequelize.sync([
  (options) => {
    operatorsAliases: "0";
  },
]);

module.exports = app;

if (app.get("env") === "development") {
  app.listen(3000, () => console.log("Server Started"));
}
